import 'package:flutter/material.dart';
import 'package:mixed_app/screens/column.dart';
import 'package:mixed_app/screens/home_screen.dart';
import 'package:mixed_app/screens/row.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mixed App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: HomePage.id,
      routes: {
        HomePage.id: (context) => HomePage(),
        ColumnWidget.id: (context) => ColumnWidget(),
        RowWidget.id: (context) => RowWidget(),
      },
    );
  }
}
