import 'package:flutter/material.dart';
import 'package:mixed_app/screens/column.dart';
import 'package:mixed_app/screens/row.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class HomePage extends StatefulWidget {
  static const String id = '/';
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final longList = List<Widget>.generate(20, (i) => Text("Item $i"));
  int hight = 180;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mixed Application"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            // Show Snack Bar
            Container(
              child: MaterialButton(
                padding: EdgeInsets.all(15),
                color: Colors.tealAccent,
                onPressed: () {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(
                        'This Snack Bar ',
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                      backgroundColor: Colors.teal,
                    ),
                  );
                },
                child: Text(
                  'Show Snack Bar',
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            // Show Buttom Sheet
            Container(
              child: ElevatedButton(
                onPressed: () {
                  showMaterialModalBottomSheet(
                    enableDrag: true,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    context: context,
                    builder: (context) => Container(
                      height: 300,
                      //width: 100,
                      child: Column(
                        //mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(height: 10),
                          Text(
                            "Button Sheet",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                          ListTile(
                            leading: CircleAvatar(
                              radius: 30,
                              child: Text("img"),
                            ),
                            title: Text("Mustaf Abubakar Abdullahi"),
                            subtitle: Text("Web Developer"),
                          ),
                          ListTile(
                            leading: CircleAvatar(
                              backgroundColor: Colors.greenAccent,
                              radius: 30,
                              child: Text("img"),
                            ),
                            title: Text("Ahmed Mohamed Abdiqadir"),
                            subtitle: Text("Mobile Developer"),
                          ),
                          ListTile(
                            leading: CircleAvatar(
                              radius: 30,
                              backgroundColor: Colors.teal,
                              child: Text("img"),
                            ),
                            title: Text("Abdirahman Mohamud Mohamed"),
                            subtitle: Text("Mobile Developer"),
                          ),
                        ],
                      ),
                    ),
                  );
                },
                child: Text("Show Modal Button Sheet"),
              ),
            ),
            // Slider
            SliderTheme(
              data: SliderTheme.of(context).copyWith(
                activeTrackColor: Colors.blue,
                thumbShape: RoundSliderThumbShape(enabledThumbRadius: 15),
                overlayColor: Color(0x29EB1555),
                overlayShape: RoundSliderOverlayShape(overlayRadius: 30),
              ),
              child: Slider(
                value: hight.toDouble(),
                min: 120,
                max: 300,
                inactiveColor: Colors.grey,
                thumbColor: Colors.yellow,
                onChanged: (double newValue) {
                  setState(() {
                    hight = newValue.round();
                    print(hight);
                  });
                },
              ),
            ),

            //Dialog
            ElevatedButton(
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (_) => AlertDialog(
                    title: Text('Alert Dialog'),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text('Hey You! I love flutter!'),
                      ],
                    ),
                    actions: <Widget>[
                      MaterialButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text('Close'),
                      )
                    ],
                  ),
                );
              },
              child: Text("Show Alert Dialog"),
            ),
            ElevatedButton(
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (_) => Dialog(
                    child: Container(
                      //color: Colors.tealAccent,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text('Dialog'),
                          Flexible(
                            child: ListView.builder(
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemCount: longList.length,
                              itemBuilder: (context, index) => longList[index],
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: MaterialButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text('Close'),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
              child: Text("Show Dialog"),
            ),
            // Go Column
            Container(
              child: MaterialButton(
                padding: EdgeInsets.all(15),
                color: Colors.tealAccent,
                onPressed: () {
                  var route = MaterialPageRoute(builder: (_) => ColumnWidget());
                  Navigator.push(context, route);
                },
                child: Text(
                  'Go to Column as Push',
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            // Go To Row by Named
            Container(
              child: MaterialButton(
                padding: EdgeInsets.all(15),
                color: Colors.tealAccent,
                onPressed: () {
                  var routeName = RowWidget.id;
                  Navigator.pushNamed(context, routeName);
                },
                child: Text(
                  'Go to Row as PushNamed',
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
