import 'package:flutter/material.dart';

class RowWidget extends StatelessWidget {
  static const String id = '/row';
  const RowWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Row Widget'),
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              color: Colors.grey[400],
            ),
            child: Row(
              children: [
                Icon(Icons.person),
                Text("Full NAME"),
              ],
            ),
          ),
          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              color: Colors.grey[300],
            ),
            child: Center(child: Text('Row 2')),
          ),
          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              color: Colors.grey[500],
            ),
            child: Center(child: Text('Row  3')),
          ),
        ],
      ),
    );
  }
}
