import 'package:flutter/material.dart';

class ColumnWidget extends StatelessWidget {
  static const String id = '/column';
  const ColumnWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Column Widget'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            CircleAvatar(
              backgroundColor: Colors.blueGrey,
              radius: 50,
              child: Text("User Img"),
            ),
            Text(
              "Saliim Dhaalle",
              style: TextStyle(
                fontSize: 18,
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.grey,
              ),
              child: Text(
                "Username",
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            //SizedBox(height: 20),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.grey,
              ),
              child: Text(
                "Email",
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            //SizedBox(height: 20),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.grey,
              ),
              child: Text(
                "Password",
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            //SizedBox(height: 20),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.grey,
              ),
              child: Text(
                "Re-Enter Password",
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.all(20),
                    textStyle: TextStyle(
                      fontSize: 20,
                    )),
                onPressed: () {},
                child: Text("Register"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
